@extends('layout.master')
@section('title')
Halaman Home
@endsection
@section('content') 
<h1>Media Online</h1>
    <h2>Sosial Media Developer </h2>
    <p>Belajar dan Berbagi agar hidup menjadi labih baik</p>
    <h2>Benefit Join di Media Online</h2>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li> 
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul> 
    <h2>Cara Bergabung ke Media Online</h2>
    <ol>
        <li> Mengunjungi website ini </li>
        <li> Mendaftarkan di  <a href="/register"> Form Sign Up </a></li>
        <li> Selesai</li>
    </ol>   
    
@endsection