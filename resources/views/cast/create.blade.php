@extends('layout.master')
@section('title')
Halaman Form Data Pemain Film
@endsection
@section('content') 

<form method="POST" action="/cast">
    @csrf
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" class="form-control" >
    
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  </div>
  
  <div class="form-group">
    <label>Umur dalam tahun</label>
    <input type="number" name="umur" class="form-control" ></div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <div class="form-group">
    <label>Biodata</label> <br>
    <textarea name="bio"  cols="30" rows="10" calss="form-control"></textarea>
  </div>
  
  
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection