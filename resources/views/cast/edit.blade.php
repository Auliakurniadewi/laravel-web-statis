@extends('layout.master')
@section('title')
Halaman Edit Data Pemain Film
@endsection
@section('content') 

<form method="POST" action="/cast/{{$cast->id}}">
    @csrf
    @method ('put')
  <div class="form-group">
    <label>Nama Cast</label>
    <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  

  <div class="form-group">
    <label>Umur dalam tahun</label>
    <input type="number" name="umur" value="{{$cast->umur}}" class="form-control" >
  </div>
  
  
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Biodata</label> <br>
    <textarea name="bio" cols="30" rows="10" calss="form-control">{{$cast->bio}}</textarea>
  </div>
      @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection